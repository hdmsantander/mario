package com.uam.mario.builder;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import com.uam.mario.entities.Block;
import com.uam.mario.entities.Level;
import com.uam.mario.entities.LogicBlock;
import com.uam.mario.entities.enums.BlockType;
import com.uam.mario.presentation.GamePanel;
import com.uam.mario.services.FileInteractionService;

public class LevelBuilder {
	
	public static final int CURSOR_SIZE = 40;

	public static final int PIPE_LENGHT = 80;
	public static final int PIPE_WIDTH = 70;
	
	private BufferedImage cursorImage;
	
	private int levelWidth;
	
	private int levelHeight;
	
	private int blockCycle;
	
	private Level level;
	
	private ArrayList<Block> blocks = new ArrayList<>();
	
	private BlockType currentBlock;
		
	private int x;
	
	private int y;
	
	private String levelName;
	
	private GamePanel gamePanel;
	
	public LevelBuilder(int levelWidth, int levelHeight, Level level, GamePanel gamePanel, String levelName) {
		this.levelWidth = levelWidth;
		this.levelHeight = levelHeight;
		this.blockCycle = 0;
		this.level = level;
		this.gamePanel = gamePanel;
		this.currentBlock = BlockType.BRICK;
		this.resetCursor();
		this.levelName = levelName;
		cursorImage = setCursorImageToBlock(this.currentBlock);
	}
	
	public void paint(Graphics g, int xOffset, int yOffset) {
		
		switch(this.currentBlock) {
		case BRICK:
		case QUESTION:
		case SMOOTH:
			g.drawImage(cursorImage, x, y,CURSOR_SIZE, CURSOR_SIZE, level.getGamePanel());
			break;
		case PIPE:
			g.drawImage(cursorImage, x, y,PIPE_WIDTH, PIPE_LENGHT, level.getGamePanel());
			break;
		}
		
		for (Block block : blocks) {
			block.paint(g, xOffset, yOffset);
		}
		g.setColor(Color.CYAN);
		g.fillRect(x, y, 10, 10);
				
	}
	
	private InputStream getLevelLayout() {
		return LevelBuilder.class.getClassLoader().getResourceAsStream(levelName);
	}
	
	public void performAction(int keyCode, int xOffset, int yOffset) {
		
		switch(keyCode) {
			
			// Create block
			case KeyEvent.VK_Q:
				blocks.add(new Block(x-xOffset,y-yOffset,currentBlock,level));
				break;
	
			case KeyEvent.VK_W:
				changeBlock();
				break;
				
			case KeyEvent.VK_E:
				resetCursor();
				break;
				
			case KeyEvent.VK_S:
				System.out.println("Writing to file!");
				ArrayList<LogicBlock> lblocks = new ArrayList<>();
				for (Block block : blocks) {
					lblocks.add(block.toLogicBlock());
				}
				LogicBlock[] lba = new LogicBlock[lblocks.size()]; 
				FileInteractionService.writeObjectToFile(lblocks.toArray(lba), levelName);
				System.out.println("Reading from file");
				LogicBlock[] blockerinos = FileInteractionService.readObjectFromFile(levelName, LogicBlock[].class);
				System.out.println("Content: " + blockerinos[0].x);				
				break;
			
			default:
				;
		}
	}
	
	private void resetCursor() {
		x=gamePanel.getWidth()/2;
		y=gamePanel.getHeight()/2+22;
	}
	
	private void changeBlock() {
		
		blockCycle = (blockCycle + 1) % 4;
		
		switch(blockCycle) {
			case 0:
				this.currentBlock = BlockType.BRICK;
				cursorImage = setCursorImageToBlock(this.currentBlock);
				break;
			case 1:
				this.currentBlock = BlockType.QUESTION;
				cursorImage = setCursorImageToBlock(this.currentBlock);
				break;
			case 2:
				this.currentBlock = BlockType.SMOOTH;
				cursorImage = setCursorImageToBlock(this.currentBlock);
				break;
			case 3:
				this.currentBlock = BlockType.PIPE;
				cursorImage = setCursorImageToBlock(this.currentBlock);
				break;
		}
	}
	
	private BufferedImage setCursorImageToBlock(BlockType type) {
		
		String imagePath;
		
		switch(type) {
		case BRICK:
			imagePath = "ladrillo.png";
			break;

		case QUESTION:
			imagePath = "question.png";
			break;

		case SMOOTH:
			imagePath = "smooth.png";
			break;

		case PIPE:
			imagePath = "pipe.png";
			break;
			
		default :
			imagePath= "ladrillo.png";
		}
		
		try {
			return ImageIO.read(GamePanel.class.getClassLoader().getResourceAsStream(imagePath));
		} catch (IOException e) {
			return null;
		}
	}
	
}
