package com.uam.mario.presentation;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.lang.module.ModuleDescriptor.Builder;

import javax.swing.JPanel;
import javax.swing.Timer;

import com.uam.mario.builder.LevelBuilder;
import com.uam.mario.entities.Level;
import com.uam.mario.entities.LevelOne;
import com.uam.mario.entities.Marianito;

public class GamePanel extends JPanel implements ActionListener, KeyListener {

	private static final long serialVersionUID = 3119476954051942138L;

	private GameFrame gameFrame;

	private LevelBuilder levelBuilder;
	
	private boolean builderMode;

	private Level level;

	private Marianito marianito;

	private Timer timer;

	public GamePanel(GameFrame gameFrame, boolean builderMode) {
		this.gameFrame = gameFrame;
		this.timer = new Timer(1, this);
		this.builderMode = builderMode;
		this.addKeyListener(this);
		this.setFocusable(true);
		if (!builderMode)
			this.marianito = new Marianito(90, 249, this);
		this.level = new LevelOne(this, "mundo-cropped.png", 670, 371, builderMode);
		this.startGame();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		level.paint(g);
		if (!builderMode)
			marianito.paint(g, level.getXOffset(), level.getYOffset());

	}

	public void startGame() {
		try {
			setVisible(true);
			timer.start();
			if(!builderMode)
			marianito.start();
			level.start();
		} catch (IllegalThreadStateException e) {

		}
	}

	public void stopGame() {
		timer.stop();
	}

	private void update() {
		if (this.isDisplayable() && this.isValid()) {
			repaint();
		}
	}

	public Level getLevel() {
		return level;
	}

	public int getWidth() {
		return gameFrame.getMaximumWidth();
	}

	public int getHeight() {
		return gameFrame.getMaximumHeight();
	}

	public Marianito getMarianito() {
		return marianito;
	}

	public void keyPressed(KeyEvent event) {
		if (!builderMode) {
			if (event.getKeyCode() == KeyEvent.VK_UP)
				marianito.jump();
			marianito.keyPressed(event.getKeyCode());
		}		
		level.keyPressed(event.getKeyCode());
	}

	public void keyReleased(KeyEvent event) {
		if(!builderMode)
			marianito.keyReleased(event.getKeyCode());
	}

	public void keyTyped(KeyEvent arg0) {

	}

	public void actionPerformed(ActionEvent arg0) {
		update();
	}

}
