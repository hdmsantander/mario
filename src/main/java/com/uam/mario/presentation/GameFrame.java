package com.uam.mario.presentation;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

public class GameFrame extends JFrame {
	
	public static final int WIDTH = 660;
	public static final int HEIGHT = 401;

	private Dimension dimensions;

	private static final long serialVersionUID = -3307506519359464480L;
	
	private GamePanel gamePanel = null;
	
	private boolean builderMode;

	public GameFrame(boolean builderMode) {
		super();
		this.builderMode = builderMode;
		this.dimensions = new Dimension(WIDTH, HEIGHT);
		setMinimumSize(dimensions);
		setMaximumSize(dimensions);
		setPreferredSize(dimensions);
		setResizable(false);
		setTitle("~ M A R I A N I T O - B R O S ~");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.gamePanel = new GamePanel(this, builderMode);
		add(gamePanel);
		pack();
		setVisible(false);
		startGame();
	}
	
	public void startGame() {
		setVisible(true);
		gamePanel.startGame();
	}
	
	public int getMaximumWidth() {
		return WIDTH;
	}
	
	public int getMaximumHeight() {
		return HEIGHT - 30;
	}
}
