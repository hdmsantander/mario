package com.uam.mario;

import com.uam.mario.presentation.GameFrame;

public class App {
	public static void main(String[] args) {
		
		boolean builderMode = false;
		if (args.length > 0) {
			builderMode = args[0].equals("builder");
		}
		new GameFrame(builderMode);
	}
}
