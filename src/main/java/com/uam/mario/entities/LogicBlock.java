package com.uam.mario.entities;

import java.io.Serializable;

import com.uam.mario.entities.enums.BlockType;

import lombok.Data;

@Data
public class LogicBlock implements Serializable{
	
	public static final long serialVersionUID = -4383676973844740301L;

	public static final int BLOCK_SIZE = 40;
	
	public BlockType type;
	
	public int x;
	
	public int y;
	
	public boolean breakable;
	
	public LogicBlock(int x, int y, BlockType type) {
		this.x = x;
		this.y = y;
		switch (type){
		case BRICK:
			this.breakable = true;
		}
	}
	
	public LogicBlock(Block block) {
		this.x = block.getX();
		this.y = block.getY();
		this.type = block.getType();
		switch (block.getType()){
		case BRICK:
			this.breakable = true;
		}
	}
	
	public Block toBlock(Level level) {
		return new Block(this,level);
	}
		
	@Override
	public String toString() {
		return new StringBuffer(type.toString()).append(",")
        .append(x).append(",").append(y).append(",").append(breakable).toString();
	}	
}
