package com.uam.mario.entities.enums;

public enum BlockType {
	BRICK, QUESTION, SMOOTH, PIPE;
}
