package com.uam.mario.entities;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import com.uam.mario.entities.interfaces.MovableComponent;
import com.uam.mario.presentation.GamePanel;
import com.uam.mario.services.MusicService;

public class Marianito extends Thread implements MovableComponent {
	
	public static final int MARIANITO_WIDTH = 40;
	public static final int MARIANITO_LENGHT = 40;
	
	public static final int MARIANITO_STRIDE = 10;
	
	public static final int JUMP_STEPS = 15;
	
	private boolean goingRight;
	
	private boolean facingRight;
	
	private boolean goingLeft;
	
	private boolean facingLeft;
	
	private int speedModifier;
	
	private boolean active;
	
	private int x;
	
	private int y;
	
	private int fallingTimer;
	
	private int jumpingTimer;
	
	private int movementTimer;
	
	private boolean jumping;
	
	private boolean falling;
	
	private int step_right;
	
	private int step_left;
	
	private MusicService jumpMusic;
	
	private BufferedImage currentSprite;
	
	private BufferedImage right_1;
	private BufferedImage right_2;
	private BufferedImage right_3;
	
	private BufferedImage left_1;
	private BufferedImage left_2;
	private BufferedImage left_3;

	private BufferedImage jump_right;
	private BufferedImage jump_left;
	
	private boolean jump;
	
	private int jump_steps;
	
	private GamePanel gamePanel;
		
	public Marianito(int x, int y, GamePanel gamePanel) {
		
		this.x = x;
		this.y = y;
		this.active = true;
		this.gamePanel = gamePanel;
		this.jump = false;
		
		this.speedModifier = 1;
		
		initializeBackgroundImages();
		
		this.fallingTimer = 1;
		this.jumpingTimer = 1;
		this.movementTimer = 1;
		
		this.jumping = false;
		this.falling = false;
		this.goingLeft = false;
		this.goingRight = false;
		this.facingRight = true;
		this.facingLeft = false;
		
		this.step_right = 0;
		this.step_left = 0;
		
		this.jump_steps = 0;
		
		jumpMusic = new MusicService("salto.aiff",0);
		initializeMusic();
	}
	
	@Override
	public void run() {
		update();
	}
	
	public void update() {
		while(active) {
			waitSomeTime(1, 0);
									
			if (!jumping) {
				fall();
				move();
			}
			else {
				jump();
			}
		}
	}
	
	private void stepRight() {
		
		facingLeft = false;
		step_right = (step_right + 1) % 2;
		
		if(canMoveRight()) {
			if (closeToRight()) {
				if (!gamePanel.getLevel().adjustLevelRight()) {
					moveRight();
				}
			} else {
				moveRight();
			}
			
			goingRight = true;
		}
			
	}
	
	private void stepLeft() {
		facingRight = false;
		step_left = (step_left +1)%2;
		if (canMoveLeft()) {
			if (closeToLeft()) {
				if (!gamePanel.getLevel().adjustLevelLeft()) {
					moveLeft();
				}
			} else {
				moveLeft();
			}
			
			goingLeft = true;
		}
	}
	
	public void jump() {
				
		if (jumpingTimer != 30) {
			waitSomeTime(1, 300);
			jumpingTimer+=1;
			return;
		}
			
		jumpingTimer = 0;
		
		int distanceToJump = MARIANITO_STRIDE;
		
		boolean hasJumped = false;
		
		while (!hasJumped) {
			
			try {
				// Checa si podemos brincar esa distancia
				if (gamePanel.getLevel().canJump(new Rectangle(x, y, MARIANITO_WIDTH, MARIANITO_LENGHT), distanceToJump)) {

					y -= distanceToJump;

					if (goingRight && canMoveRight()) {
						stepRight();
					}

					if (goingLeft && canMoveLeft()) {
						stepLeft();
					}

					hasJumped = true;
					jump_steps = (jump_steps + 1) % JUMP_STEPS;

				} else {
					// Si no, hay que reducir la distancia
					distanceToJump--;
				}
			} catch (RuntimeException e) {
				// Un bloque fue roto
				distanceToJump = 0;
			}
			
		}
		
		// Si ya se acabó el ciclo de brinco ya no estamos brincando
		if(jump_steps == JUMP_STEPS - 1 || distanceToJump == 0) {
			jumping=false;
			falling=true;
			jump_steps = 0;
		}
		
	}
	
	
	
	public void keyPressed(int keyCode) {
		
		switch(keyCode) {
		
		case KeyEvent.VK_LEFT:
			goingLeft = true;
			break;

		case KeyEvent.VK_RIGHT:
			goingRight = true;
			break;

		case KeyEvent.VK_DOWN:
			break;

		case KeyEvent.VK_UP:
			if (isStandingInSomething()) {
				jumping = true;
				jumpMusic.start();
				jump();
			}
			break;
		}
		
	}
	
	public void keyReleased(int keyCode) {
		switch(keyCode) {
		
		case KeyEvent.VK_LEFT:
			goingLeft=false;
			facingLeft=true;
			break;

		case KeyEvent.VK_RIGHT:
			goingRight=false;
			facingRight=true;
			break;

		case KeyEvent.VK_DOWN:
			break;

		case KeyEvent.VK_UP:
			break;
		}
	}
	
	private BufferedImage determineSprite() {
				
		if ((jumping || falling) && (goingRight || facingRight)) {
			return jump_right;
		}
		
		if ((jumping || falling) && (goingLeft || facingLeft)) {
			return jump_left; 
		}
		
		if (goingRight) {
			return step_right == 1 ? right_2 : right_3;
		}
		
		if (goingLeft) {
			return step_left == 1 ? left_2 : left_3;
		}
		
		if (facingLeft) {
			return left_1;
		}
		
		return right_1;
	}
	
	
	
	public boolean closeToRight() {
		return x > gamePanel.getWidth()/5 * 3 ;
	}
	
	public boolean closeToLeft() {
		return x < gamePanel.getWidth()/5 * 2;
	}
	
	public void paint(Graphics g, int xOffset, int yOffset) {
		currentSprite = determineSprite();
		g.drawImage(currentSprite, x, y, MARIANITO_WIDTH, MARIANITO_LENGHT, gamePanel.getLevel().getGamePanel());
	}
	
	public void waitSomeTime(long milis, int nanos) {
		try {
			if (this.isAlive())
				Thread.sleep(milis, nanos);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
		
	private void initializeBackgroundImages() {
		try {
			 right_1 = ImageIO.read(GamePanel.class.getClassLoader().getResourceAsStream("marianito_right_1.png"));
			 right_2 = ImageIO.read(GamePanel.class.getClassLoader().getResourceAsStream("marianito_right_2.png"));
			 right_3 = ImageIO.read(GamePanel.class.getClassLoader().getResourceAsStream("marianito_right_3.png"));
			 
			 left_1 = ImageIO.read(GamePanel.class.getClassLoader().getResourceAsStream("marianito_left_1.png"));
			 left_2 = ImageIO.read(GamePanel.class.getClassLoader().getResourceAsStream("marianito_left_2.png"));
			 left_3 = ImageIO.read(GamePanel.class.getClassLoader().getResourceAsStream("marianito_left_3.png"));
			 
			 jump_right = ImageIO.read(GamePanel.class.getClassLoader().getResourceAsStream("salta_derecha.png"));
			 jump_left = ImageIO.read(GamePanel.class.getClassLoader().getResourceAsStream("salta_izquierda.png"));
			 
		} catch (IOException e) {
			System.out.println("Error cargando imágenes");
			System.exit(1);
		}
	}
	
	private void fall() {
		
		if (fallingTimer <= 15) {
			waitSomeTime(2, 300);
			fallingTimer+=1;
			return;
		}
			
		fallingTimer = 0;
		
		// Solo caemos si no hay piso debajo
		if (!gamePanel.getLevel().theresAFloorBelow(getBounds())) {
			
			// Primero revisamos la distancia que "debemos caer"
			int distanceToFall = gamePanel.getLevel().getFallingSpeed();
			boolean hasFallen = false;
			
			// Cae lo que se pueda caer
			while (!hasFallen) {
				
				// Checa si podemos caer esa distancia
				if (!gamePanel.getLevel().theresAFloorBelow(new Rectangle(x,y+distanceToFall,MARIANITO_WIDTH,MARIANITO_LENGHT))) {
					y+=distanceToFall;
					hasFallen=true;
				} else {
					
					// Si no, hay que reducir la distancia
					distanceToFall--;
				}
			}
			
			if (distanceToFall ==0 && falling) {
				falling = false;
			}
			 
		}
	}
	
	public Rectangle getBounds() {
		return new Rectangle(x,y,MARIANITO_WIDTH,MARIANITO_LENGHT);
	}

	@Override
	public boolean canMoveLeft() {
		return x > 0 && gamePanel.getLevel().marioCanMakeAValidMovement(new Rectangle(x-MARIANITO_STRIDE * speedModifier,y,MARIANITO_WIDTH,MARIANITO_LENGHT));
	}

	@Override
	public void moveLeft() {
		x-=MARIANITO_STRIDE * speedModifier;
		
	}

	@Override
	public boolean canMoveRight() {
		return gamePanel.getLevel().marioCanMakeAValidMovement(new Rectangle(x+MARIANITO_STRIDE * speedModifier,y,MARIANITO_WIDTH,MARIANITO_LENGHT));
	}

	@Override
	public void moveRight() {
		x+=MARIANITO_STRIDE * speedModifier;
	}

	@Override
	public boolean canMoveUp() {
		return true;
	}

	@Override
	public void moveUp() {
		y-=5;
	}

	@Override
	public boolean canMoveDown() {
		return true;
	}

	@Override
	public void moveDown() {
		y+=5;
	}

	@Override
	public void move() {
				
		if(movementTimer <= 15) {
			waitSomeTime(1, 300);
			movementTimer+=1;
			return;
		}
		
		movementTimer = 0;
		
		if(goingRight) {
			stepRight();
		}
		
		if(goingLeft) {
			stepLeft();
		}
		
	}
	
	private boolean isStandingInSomething() {
		return gamePanel.getLevel().theresAFloorBelow(new Rectangle(x,y+1,MARIANITO_WIDTH,MARIANITO_LENGHT));
	}
	
	private void initializeMusic() {
		try {
			jumpMusic.initializeTrack();
		} catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
			System.out.println("Can't play music");
		}
		
	}
	
}
