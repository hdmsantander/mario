package com.uam.mario.entities;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.imageio.ImageIO;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import com.uam.mario.builder.LevelBuilder;
import com.uam.mario.entities.interfaces.MovableComponent;
import com.uam.mario.presentation.GamePanel;
import com.uam.mario.services.FileInteractionService;
import com.uam.mario.services.MusicService;

public abstract class Level extends Thread implements MovableComponent {
	
	private GamePanel gamePanel;
	
	private String levelBackground;
	
	private String levelName;
	
	private BufferedImage gameImage;
	
	private Optional<LevelBuilder> builder;
	
	public List<Block> blocks;
	
	public int fallingSpeed;
	
	public int backgroundWidth;
	
	public int backgroundHeight;
	
	private boolean isActive;
	
	public int floorLevel;
	
	public int xOffset;
	
	public int yOffset;
	
	private int imageX;
	
	private int imageY;
	
	private int levelMovingSpeed;
	
	private MusicService levelMusic;
	
	private MusicService ladrilloMusic;
	private MusicService deathMusic;
	private MusicService gameOverMusic;
		
	public Level(GamePanel gamePanel, String levelBackground, String levelName, int backgroundWidth, int backgroundHeight, boolean levelBuilder) {
		this.gamePanel = gamePanel;
		this.levelBackground = levelBackground;
		this.gameImage = initializeBackgroundImage();
		this.backgroundWidth = backgroundWidth;
		this.backgroundHeight = backgroundHeight;
		this.levelName = levelName;
		this.levelMovingSpeed = 5;
		this.isActive = true;
		this.imageX = 0;
		this.imageY = 0;
		this.xOffset = 0;
		this.yOffset = 0;
		if (levelBuilder) {
			this.builder = Optional.of(new LevelBuilder(backgroundWidth, backgroundHeight, this, gamePanel, "mundo1"));
		} else {
			this.builder = Optional.ofNullable(null);
			this.blocks = loadLevelResources();
			levelMusic = new MusicService(levelName+".aiff", Clip.LOOP_CONTINUOUSLY);
			ladrilloMusic = new MusicService("ladrillo.aiff", 0);
			deathMusic = new MusicService("muerte.aiff", 0);
			gameOverMusic = new MusicService("game_over.aiff", 0);
			startMusic();
		}
	}
	
	public void paint(Graphics g) {
		g.drawImage(gameImage, imageX, imageY, gamePanel);
		
		if (builder.isPresent()) {
			builder.get().paint(g, xOffset, yOffset);
		} else {
			for (Block block : blocks) {
				block.paint(g, xOffset, yOffset);
			}
		}
	}
	
	@Override
	public void run() {
		update();
	}
	
	private void update() {
		while(isActive) {
			waitSomeTime(1, 0);
		}
	}
	
	public boolean canJump(Rectangle r, int verticalIncrement) {
		
		for(Iterator<Block> iterator = blocks.iterator() ; iterator.hasNext() ;) {
			
			Block block = iterator.next();
			
			if (block.getBounds(xOffset, yOffset).intersects(new Rectangle(r.x,r.y-verticalIncrement,r.width,verticalIncrement))) {
				
				if(block.isBreakable()) {
					iterator.remove();
					ladrilloMusic.start();
					throw new RuntimeException("Block broken");
				}
				return false;
			}
			
		}
		
		return true;		
	}
	
	public void keyPressed(int keyCode) {
		if (builder.isPresent()) {
			builder.get().performAction(keyCode, xOffset, yOffset);
			determineMovement(keyCode);
		}
	}
	
	public int getFallingSpeed() {
		return fallingSpeed;
	}
		
	public int getXOffset() {
		return xOffset;
	}
	
	public int getYOffset() {
		return yOffset;
	}
	
	public abstract boolean theresAFloorBelow(Rectangle r);
	
	public void determineMovement(int keyCode) {
		
		switch(keyCode) {
		
		case KeyEvent.VK_LEFT:
			moveLeft();
			break;

		case KeyEvent.VK_RIGHT:
			moveRight();
			break;

		case KeyEvent.VK_DOWN:
			moveDown();
			break;

		case KeyEvent.VK_UP:
			moveUp();
			break;
		}
		
	}
	
	public boolean marioCanMakeAValidMovement(Rectangle r) {
				
		for (Block block : blocks) {
			if (block.getBounds(xOffset, yOffset).intersects(r)) {
				return false;
			}
		}
		
		return true;
	}
	
	public boolean adjustLevelRight() {
		if (canMoveRight()) {
			moveRight();
			return true;
		} else {
			return false;
		}
		
	}
	
	public boolean adjustLevelLeft() {
		if (canMoveLeft()) {
			moveLeft();
			return true;
		} else {
			return false;
		}
	}
	
	public GamePanel getGamePanel() {
		return this.gamePanel;
	}
	
	@Override
	public boolean canMoveLeft() {
		return imageX != 0;
	}
	
	@Override
	public void moveLeft() {
		if (builder.isPresent()) {
			imageX-=LevelBuilder.CURSOR_SIZE;
			xOffset-=LevelBuilder.CURSOR_SIZE;
		} else {
			imageX+=Marianito.MARIANITO_STRIDE;
			xOffset+=Marianito.MARIANITO_STRIDE;
		}
		
	}
	
	@Override
	public boolean canMoveRight() {
		return imageX != backgroundWidth;
	}
	
	@Override
	public void moveRight() {
		if (builder.isPresent()) {
			imageX+=LevelBuilder.CURSOR_SIZE;
			xOffset+=LevelBuilder.CURSOR_SIZE;
		} else {
			imageX-=Marianito.MARIANITO_STRIDE;
			xOffset-=Marianito.MARIANITO_STRIDE;
		}
		
	}
	
	@Override
	public boolean canMoveUp() {
		return true;
	}
	
	@Override
	public void moveUp() {
		if (builder.isPresent()) {
			imageY-=LevelBuilder.CURSOR_SIZE;
			yOffset-=LevelBuilder.CURSOR_SIZE;
		}
	}
	
	@Override
	public boolean canMoveDown() {
		return true;
	}
	
	@Override
	public void moveDown() {
		if (builder.isPresent()) {
			imageY+=LevelBuilder.CURSOR_SIZE;
			yOffset+=LevelBuilder.CURSOR_SIZE;
		}
	}
	
	private void startMusic() {
		try {
			levelMusic.initializeTrack();
			ladrilloMusic.initializeTrack();
			deathMusic.initializeTrack();
			gameOverMusic.initializeTrack();
		} catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
			System.out.println("Can't play music");
		}
		
	}
	
	@Override
	public void move() {
		
	}
	
	private BufferedImage initializeBackgroundImage() {
		try {
			return ImageIO.read(GamePanel.class.getClassLoader().getResourceAsStream(levelBackground));
		} catch (IOException e) {
			return null;
		}
	}
	
	private ArrayList<Block> loadLevelResources() {
		ArrayList<Block> loadedBlocks = new ArrayList<>();
		LogicBlock[] lba = FileInteractionService.readObjectFromFile(levelName, LogicBlock[].class);
		for (LogicBlock lb : lba) {
			loadedBlocks.add(lb.toBlock(this));
		}
		return loadedBlocks;
	}
	
	public void waitSomeTime(long milis, int nanos) {
		try {
			Thread.sleep(milis, nanos);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
