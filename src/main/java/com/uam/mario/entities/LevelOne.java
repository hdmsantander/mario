package com.uam.mario.entities;

import java.awt.Rectangle;

import com.uam.mario.presentation.GamePanel;

public class LevelOne extends Level {
		
	public LevelOne(GamePanel gamePanel, String levelBackground, int backgroundWidth, int backgroundHeight, boolean levelBuilder) {
		
		super(gamePanel,levelBackground,"mundo1",backgroundWidth,backgroundHeight, levelBuilder);
		this.floorLevel = 288;
		this.fallingSpeed = 9;
		this.backgroundWidth = -1640;
		
	}
	
	public int getFloorLevel() {
		return floorLevel;
	}
	
	@Override
	public boolean theresAFloorBelow(Rectangle r) {
		
		for (Block block : blocks) {
			if ((r.x + 40 > block.getX() || (r.x + block.getWidth()) < block.getX()) && r.intersects(block.getBounds(xOffset,yOffset))) {
				return true;
			}
		}
		
		return r.intersects(getFloorRectangleAtPosition(r));
		
	}
	
	private Rectangle getFloorRectangleAtPosition(Rectangle r) {
		return new Rectangle(r.x,floorLevel,40,40*2);
	}
}
