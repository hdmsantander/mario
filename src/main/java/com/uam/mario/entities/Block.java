package com.uam.mario.entities;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.Serializable;

import javax.imageio.ImageIO;

import com.uam.mario.entities.enums.BlockType;
import com.uam.mario.presentation.GamePanel;

import lombok.Data;

@Data
public class Block implements Serializable{
	
	private static final long serialVersionUID = -4383676973844740301L;

	public static final int BLOCK_SIZE = 40;
	
	public static final int PIPE_LENGHT = 80;
	public static final int PIPE_WIDTH = 70;
	
	private BlockType type;
	
	private int x;
	
	private int y;
	
	private String imagePath;
	
	private BufferedImage image;
	
	private boolean breakable;
	
	private Level level;
	
	public Block(LogicBlock logicBlock, Level level) {
		this.x = logicBlock.x;
		this.y = logicBlock.y;
		this.type = logicBlock.type;
		this.level = level;
		initializeBlock(logicBlock.type);
		this.image = initializeBackgroundImage();
		
	}
	
	public Block(int x, int y, BlockType type, Level level) {
		this.x = x;
		this.y = y;
		this.type = type;
		this.level = level;
		initializeBlock(type);
		this.image = initializeBackgroundImage();
	}
		
	@Override
	public String toString() {
		return new StringBuffer(type.toString()).append(",")
        .append(x).append(",").append(y).toString();
	}
	
	public void paint(Graphics g, int xOffset, int yOffset) {
		switch(this.type) {
			case BRICK:
			case QUESTION:
			case SMOOTH:
				g.drawImage(image, x+xOffset, y+yOffset,BLOCK_SIZE, BLOCK_SIZE, level.getGamePanel());
				break;
			case PIPE:
				g.drawImage(image, x+xOffset, y+yOffset,PIPE_WIDTH, PIPE_LENGHT, level.getGamePanel());
				break;
		}		
	}
	
	public Rectangle getBounds(int xOffset, int yOffset) {
		return this.type == BlockType.PIPE ? new Rectangle(x+xOffset,y+yOffset,PIPE_WIDTH,PIPE_LENGHT) : new Rectangle(x+xOffset,y+yOffset,BLOCK_SIZE,BLOCK_SIZE);
	}
	
	public int getWidth() {
		return this.type == BlockType.PIPE ? PIPE_WIDTH:BLOCK_SIZE;
	}
	
	private void initializeBlock(BlockType type) {
		switch (type){
		
			case BRICK:
				this.breakable = true;
				this.imagePath = "ladrillo.png";
				break;
	
			case QUESTION:
				this.breakable = false;
				this.imagePath = "question.png";
				break;
	
			case SMOOTH:
				this.breakable = false;
				this.imagePath = "smooth.png";
				break;
	
			case PIPE:
				this.breakable = false;
				this.imagePath = "pipe.png";
				break;
		}
	}
	
	private BufferedImage initializeBackgroundImage() {
		try {
			return ImageIO.read(GamePanel.class.getClassLoader().getResourceAsStream(imagePath));
		} catch (IOException e) {
			return null;
		}
	}
	
	public LogicBlock toLogicBlock() {
		return new LogicBlock(this);
	}
	
}
