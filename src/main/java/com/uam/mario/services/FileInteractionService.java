package com.uam.mario.services;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class FileInteractionService {
	
	@SuppressWarnings("unchecked")
	public static <T> T readObjectFromFile(String filePath, Class<T> objectType) {
		FileInputStream fi;
		try {
			fi = new FileInputStream(filePath);
			@SuppressWarnings("resource")
			// TODO, fix resource leak
			ObjectInputStream oi = new ObjectInputStream(fi);
			return (T) oi.readObject();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static void writeObjectToFile(Object object, String filePath) {
        try {
 
            FileOutputStream fileOut = new FileOutputStream(filePath);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(object);
            objectOut.close();
            System.out.println("The Object  was succesfully written to a file");
 
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
