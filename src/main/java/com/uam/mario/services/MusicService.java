package com.uam.mario.services;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class MusicService {

	private Clip clip;
	private AudioInputStream audioInputStream = null;
	private String musicPath;
	private int playMode;

	public MusicService(String musicPath, int playMode) {
		this.musicPath = musicPath;
		this.playMode = playMode;
	}

	public void initializeTrack()
			throws UnsupportedAudioFileException, IOException, LineUnavailableException {

		if (audioInputStream != null) {
			audioInputStream.close();
			clip.stop();
			clip.close();
		}

		this.clip = AudioSystem.getClip();

		InputStream is = MusicService.class.getClassLoader().getResourceAsStream(musicPath);

		this.audioInputStream = AudioSystem.getAudioInputStream(new BufferedInputStream(is));

		this.clip.open(audioInputStream);
		
		if(playMode == Clip.LOOP_CONTINUOUSLY)
			this.clip.loop(playMode);
	}

	public void start() {
		clip.start();
	}
	
	public void stop() {
		clip.stop();
	}
	
}
